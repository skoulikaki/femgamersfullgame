﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoxArea : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(this.tag))
        {
            Debug.Log("Correct : " + collision.tag + " in "  + this.tag);
        }
        else
        {
            Debug.Log("Wrong : " + collision.tag + " in " + this.tag);
        }
    }
}
