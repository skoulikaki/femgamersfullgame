﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulletable : MonoBehaviour
{
    public float initialForce = 1f;
    private Rigidbody2D bulletRigidBody;

    private void Awake()
    {
        bulletRigidBody = GetComponent<Rigidbody2D>();
    }

    internal void ShootAsBullet()
    {
        bulletRigidBody.simulated = true;
        bulletRigidBody.AddForce(transform.up * initialForce, ForceMode2D.Impulse);
    }
}
