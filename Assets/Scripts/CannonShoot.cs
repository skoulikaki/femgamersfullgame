﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonShoot : MonoBehaviour
{
    public float angle = 30f;
    public float rotationSpeed = 1f;

    public Bulletable[] objectsToSpawn;

    public Transform spawnPosition;
    public Transform movableObjects;

    private Bulletable instanceOfBullet;

    // Update is called once per frame
    void Update()
    {
        RotateCannon();
        SpawnBullet();
    }

    private void SpawnBullet()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (instanceOfBullet == null)
            {
                instanceOfBullet = Instantiate(objectsToSpawn[UnityEngine.Random.Range(0, objectsToSpawn.Length)], spawnPosition);
            }
            else
            {
                instanceOfBullet.transform.SetParent(movableObjects, worldPositionStays: true);
                instanceOfBullet.ShootAsBullet();
                instanceOfBullet = null;
            }
        }
    }

    private void RotateCannon()
    {
        var newAngleZ = Mathf.PingPong(Time.time * rotationSpeed, angle * 2) - angle;
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, newAngleZ);
    }
}
